Name:		oeAware-collector
Version:	v1.0.2
Release:	11
Summary:	%{name} provides low-overhead metrics collection capabilities, including microarchitecture, system, and kernel information.
License:	MulanPSL2
URL:		https://gitee.com/openeuler/%{name}
Source0:	%{name}-%{version}.tar.gz
Patch1:		0001-add-netif_rx-counting-pmu-instance.patch
Patch2:		0002-reduce-thread-collector-load.patch
Patch3:		0003-fix-uninitialized-PmuAttr-attr.patch
Patch4:     0004-fix-out-of-bounds-array.patch
Patch5:     0005-adapt-for-4.19-kernel.patch

%global libkperf_name libkperf
%global libkperf_tagver v1.0.5
%global libkperf_source https://gitee.com/openeuler/libkperf.git

BuildRequires: cmake gcc-c++
BuildRequires: numactl-devel
BuildRequires: git
BuildRequires: libboundscheck
Requires:      libboundscheck
Provides:      %{name}

ExclusiveArch: aarch64 x86_64

%description
%{name} provides low-overhead metrics collection capabilities, including microarchitecture, system, and kernel information.

%prep
%autosetup -p1 -n %{name}-%{version}
%ifarch aarch64
cd ./pmu/
git clone --recurse-submodules %{libkperf_source}
cd %{libkperf_name}
git checkout %{libkperf_tagver}
cd ..
mv %{libkperf_name}/ %{libkperf_name}-%{libkperf_tagver}/
cd ..
%endif


%build
# build libkperf.so
%ifarch aarch64
cd %{_builddir}/%{name}-%{version}/pmu/
mkdir -p ./3rdlib/
sh ./%{libkperf_name}-%{libkperf_tagver}/build.sh
cp -r %{libkperf_name}-%{libkperf_tagver}/output/* ./3rdlib/
%endif

# build libpmu.so
%ifarch aarch64
cd %{_builddir}/%{name}-%{version}/pmu/
mkdir build
cd build
cmake .. -DLIB_KPERF_LIBPATH=%{_builddir}/%{name}-%{version}/pmu/3rdlib/bin/ \
         -DLIB_KPERF_INCPATH=%{_builddir}/%{name}-%{version}/pmu/3rdlib/include/ \
         -DCMAKE_SKIP_RPATH=TRUE -DWITH_DEBUG=TRUE
make
%endif

# build libthread_collector.so
cd %{_builddir}/%{name}-%{version}/thread_collector/
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Debug
make


%install
mkdir -p ${RPM_BUILD_ROOT}%{_libdir}/oeAware-plugin/
%ifarch aarch64
install -b -m740  ./pmu/3rdlib/bin/*.so     ${RPM_BUILD_ROOT}%{_libdir}
install -b -m740  ./pmu/build/libpmu.so     ${RPM_BUILD_ROOT}%{_libdir}/oeAware-plugin/
%endif
install -b -m740  ./thread_collector/build/libthread_collector.so     ${RPM_BUILD_ROOT}%{_libdir}/oeAware-plugin/

%files
%defattr (-, root, root)
%ifarch aarch64
%attr(0440, root, root) %{_libdir}/libkperf.so
%attr(0440, root, root) %{_libdir}/libsym.so
%attr(0440, root, root) %{_libdir}/oeAware-plugin/libpmu.so
%endif
%attr(0440, root, root) %{_libdir}/oeAware-plugin/libthread_collector.so

%changelog
* Tue Sep 10 2024 liuchanggeng <liuchanggeng@huawei.com> - v1.0.2-11
- update libkperf version to v1.0.5, to solve the perf cycles error

* Tue Aug 6 2024 liuchanggeng <liuchanggeng@huawei.com> - v1.0.2-10
- adapt for 4.19 kernel

* Tue Jul 16 2024 fly_1997 <flylove7@outlook.com> - v1.0.2-9
- fix pmu cmake build option error

* Tue Jul 16 2024 Zhao Mengmeng <zhaomengmeng@kylinos.cn> - v1.0.2-8
- explicit add libboundscheck as Requires

* Mon Jul 15 2024 fly_1997 <flylove7@outlook.com> - v1.0.2-7
- fix out of bounds array

* Fri Jul 5 2024 zhoukaiqi <zhoukaiqi@huawei.com> - v1.0.2-6
- fix uninitialized PmuAttr attr

* Tue Jul 2 2024 fly_1997 <flylove7@outlook.com> - v1.0.2-5
- reduce thread collector load

* Thu Jun 27 2024 zhoukaiqi <zhoukaiqi@huawei.com> - v1.0.2-4
- update libkperf version to v1.0.4

* Thu Jun 20 2024 liuchanggeng <liuchanggeng@huawei.com> - v1.0.2-3
- add netif_rx counting pmu instance

* Wed Jun 5 2024 zhoukaiqi <zhoukaiqi@huawei.com> - v1.0.2-2
- update libkperf version to v1.0.3

* Fri May 31 2024 zhoukaiqi <zhoukaiqi@huawei.com> - v1.0.2-1
- refactor interface and fix some bugs

* Wed May 15 2024 zhoukaiqi <zhoukaiqi@huawei.com> - v1.0.1-2
- fix compile warnings

* Sat May 11 2024 fly_1997 <flylove7@outlook.com> -v1.0.1-1
- update version to v1.0.1

* Wed May 8 2024 zhoukaiqi <zhoukaiqi@huawei.com> - v1.0.0-5
- replace enable and disable API in reflash_ring_buf

* Tue Apr 30 2024 zhoukaiqi <zhoukaiqi@huawei.com> - v1.0.0-4
- remove pmu.h and pcerrc.h

* Mon Apr 29 2024 zhoukaiqi <zhoukaiqi@huawei.com> - v1.0.0-3
- fix log while not supporting pmu uncore

* Thu Apr 18 2024 liuchanggeng <liuchanggeng@huawei.com> - v1.0.0-2
- add debug package

* Wed Apr 17 2024 liuchanggeng <liuchanggeng@huawei.com> - v1.0.0-1
- Package init
